# GroupItemList
Plugin for Runelite used to compress the right click list down to one entry per item, displaying a quantity if duplicates were found.

# Result

![Image of plugin](https://i.imgur.com/kXFE0cd.png)

